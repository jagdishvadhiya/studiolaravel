-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2020 at 08:54 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studio_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `image_master`
--

CREATE TABLE `image_master` (
  `im_id` int(10) UNSIGNED NOT NULL,
  `im_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `im_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `im_album_id` int(11) NOT NULL DEFAULT 0,
  `im_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `im_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `im_updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_master`
--

INSERT INTO `image_master` (`im_id`, `im_type`, `im_name`, `im_album_id`, `im_order`, `im_created_at`, `im_updated_at`) VALUES
(37, 'user', '415573185image20.gif', 0, '0', '2020-08-21 22:37:44', NULL),
(38, 'user', '12643560640.png', 0, '0', '2020-08-21 22:57:23', NULL),
(39, 'user', '884138046logo.png', 0, '0', '2020-08-22 06:18:36', NULL),
(41, 'user', '448968387qrolic-logo.png', 0, '0', '2020-08-22 06:30:06', NULL),
(50, 'user', 'album_1598098919_image20.gif', 0, '0', '2020-08-22 12:21:59', NULL),
(51, 'user', 'album_1598099271_0.png', 0, '0', '2020-08-22 12:27:51', NULL),
(165, 'album', '22_album_42p9twwobb_img-1.jpg', 16, '1', '2020-08-24 06:53:54', NULL),
(166, 'album', '22_album_50h1ynek80_img-10.jpg', 16, '2', '2020-08-24 06:53:54', NULL),
(167, 'album', '22_album_cqqlkt9p7o_img-6.jpg', 16, '3', '2020-08-24 06:53:54', NULL),
(168, 'album', '22_album_e3awazx1dn_img-4.jpg', 16, '4', '2020-08-24 06:53:54', NULL),
(169, 'album', '22_album_fdty9gf8fl_img-9.jpg', 16, '5', '2020-08-24 06:53:54', NULL),
(170, 'album', '22_album_iob5rah2b8_img-2.jpg', 16, '6', '2020-08-24 06:53:54', NULL),
(171, 'album', '22_album_nllxx5pzaa_img-7.jpg', 16, '7', '2020-08-24 06:53:54', NULL),
(172, 'album', '22_album_olejf4o2u4_img-3.jpg', 16, '8', '2020-08-24 06:53:54', NULL),
(173, 'album', '22_album_redgwi56xi_img-8.jpg', 16, '9', '2020-08-24 06:53:54', NULL),
(174, 'album', '22_album_rqco1e77ve_img-5.jpg', 16, '10', '2020-08-24 06:53:54', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image_master`
--
ALTER TABLE `image_master`
  ADD PRIMARY KEY (`im_id`),
  ADD UNIQUE KEY `image_master_im_name_unique` (`im_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image_master`
--
ALTER TABLE `image_master`
  MODIFY `im_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
