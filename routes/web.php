<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('backend.login'));
 });
Route::get('admin/login', "loginController@index")->name('backend.login');
Route::post('/login', "loginController@login")->name('backend.login.form');
Route::get('admin/logout', "loginController@logout")->name('backend.logout');



Route::group(["prefix" => "admin","middleware"=>['loginAuth']],function(){
    Route::get('/', function () {
       return redirect(route('backend.login'));
    });
    // genral route 
    Route::post("/studio/list","genralController@getStudioIdName")->name('backend.studio.list');

    // dashboard route 
    Route::get('/dashboard', "dashboardController@index")->name('backend.dashboard');
    Route::get('/studio', "studioController@index")->name('backend.studio');
    Route::get('/studio/view', "studioController@view")->name('backend.studio.view');
    Route::post('studio/datatable', "studioController@index")->name('backend.studio.datatable');
    Route::get('/studio/edit/{id}', "studioController@edit")->name('backend.studio.edit');
    Route::post('/studio/delete/{id}', "studioController@delete")->name('backend.studio.delete');
    Route::get('/studio/add', "studioController@add")->name('backend.studio.add');
    Route::post('/studio/add/form', "studioController@addForm")->name('backend.studio.add.form');
    Route::post('/studio/edit/form', "studioController@editForm")->name('backend.studio.edit.form');
    Route::post('/studio/add/upload', "studioController@uploadLogo")->name('studio.add.fileupload');
    // studio admin route 
   //  Route::get('/admin', "adminController@index")->name('backend.admin');
    

    // studio - admin routs
    Route::get('/studio/admin', "adminController@index")->name('backend.studio.admin');
    Route::get('/studio/admin/add', "adminController@add")->name('backend.studio.admin.add');
    Route::post('/studio/admin/addForm', "adminController@addForm")->name('backend.studio.admin.add.form');
    Route::get('/studio/admin/edit/{id}', "adminController@edit")->name('backend.studio.admin.edit');
    Route::post('/studio/admin/delete/{id}', "adminController@delete")->name('backend.studio.admin.delete');
    Route::post('studio/admin/datatable', "adminController@index")->name('backend.studio.admin.datatable');

       // studio - Album routs
       Route::get('/studio/album', "albumController@index")->name('backend.studio.album');
       Route::post('/studio/album/datatable', "albumController@index")->name('backend.studio.album.datatable');
       Route::get('/studio/album/add', "albumController@add")->name('backend.studio.album.add');
       Route::post('/studio/album/addForm', "albumController@addForm")->name('backend.studio.album.addForm');
       Route::post('/studio/album/addForm/dropzone', "albumController@dropzone")->name('backend.studio.album.addForm.dropzone');
       Route::get('/studio/album/add/fetchimage', "albumController@fetchImages")->name('backend.studio.album.addForm.fetchimages');
       Route::any('/studio/album/fetchimage/', "albumController@fetchImages")->name('backend.studio.album.addForm.fetchimages');
       Route::post('/studio/album/addForm/deleteimage', "albumController@deleteImages")->name('backend.studio.album.addForm.deleteimages');
       Route::post('/studio/album/edit', "albumController@edit")->name('backend.studio.album.edit');
       Route::post('/studio/album/delete', "albumController@delete")->name('backend.studio.album.delete');

    
});
Route::group(["prefix" => "superadmin","middleware"=>['loginAuth']],function(){
   Route::get('/', function () {
      return "super";
   });
    // Super admin route 
    Route::get('/admin', "adminController@index")->name('backend.admin');
});