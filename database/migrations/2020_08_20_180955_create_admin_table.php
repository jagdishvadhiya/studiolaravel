<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_master', function (Blueprint $table) {
            $table->integerIncrements("am_id");
            $table->string("am_name");
            $table->string("am_email");
            $table->text("am_password");
            $table->integer("am_studio_id")->default(0);
            $table->integer("am_role_id")->default(1);
            $table->integer("am_image_id")->default(NULL);
            $table->string("am_status")->default('active');
            $table->timestamp("am_created_at")->useCurrent();
            $table->timestamp("am_updated_at")->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin__master');
    }
}
