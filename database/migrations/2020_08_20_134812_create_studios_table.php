<?php

use App\Studio;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('studio_master', function (Blueprint $table) {
            
            $table->integerIncrements("sm_id");
            $table->string("sm_name");
            $table->text("sm_description");
            $table->integer("sm_logo_image_id");
            $table->string("sm_status")->default("active");
            $table->integer("sm_admin_id");
            $table->timestamp("sm_created_at")->useCurrent();
            $table->timestamp("sm_updated_at")->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studios');
    }
}
