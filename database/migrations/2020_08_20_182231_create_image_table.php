<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_master', function (Blueprint $table) {
            $table->integerIncrements("im_id");
            $table->string("im_type")->default('user');
            $table->string("im_name")->unique()->nullable(false);
            $table->integer("im_album_id")->default(0);
            $table->string("im_order")->default(0);
            $table->timestamp("im_created_at")->useCurrent();
            $table->timestamp("im_updated_at")->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image__master');
    }
}
