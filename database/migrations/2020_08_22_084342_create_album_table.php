<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_master', function (Blueprint $table) {
            $table->integerIncrements("alm_id");
            $table->integer("alm_studio_id")->nullable(false);
            $table->string("alm_name",100)->default(null);
            $table->text("alm_description")->nullable();
            $table->string("alm_status",15)->default('active');
            $table->string("alm_link",200);
            $table->string("alm_key",100)->nullable();
            $table->string("alm_password",100)->nullable();
            $table->timestamp("alm_created_at")->useCurrent();
            $table->timestamp("alm_updated_at")->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'))->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_mater');
    }
}
