<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Studio;
use Faker\Generator as Faker;

$factory->define(Studio::class, function (Faker $faker) {
    return [
        "sm_name" =>$faker->name,
        "sm_description" =>$faker->sentence(),
        "sm_logo_image_id" =>"1",
        "sm_admin_id" =>'1',
    ];
});
