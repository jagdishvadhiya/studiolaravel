@extends('back-end.layouts.master')
@section('subHeader')
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Studio</span>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Admin</span>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
@endsection

@section('content')

<div class="col-12">
    <div class="card card-custom">
        <div class="card-header">
         <h3 class="card-title">
          @if  (isset($studio))
              Edit Studio
          @else
         Add New Studio Admin 
         @endif
         </h3>
        </div>
        <!--begin::Form-->
       
           
         <div class="card-body">
            <div class="col bg-dark">
                <h1 class="text-white">Studio Details</h1>
             </div>
    
             @if  (isset($studio))
              <form class="form" id="studioAdminFormUpdate" name="studioAdminFormUpdate" method="post" action="{{route('backend.studio.edit.form')}}" enctype="multipart/form-data">
            @else
            <form class="form" id="studioAdminForm" name="studioAdminForm" method="post" action="{{route('backend.studio.admin.add.form')}}" enctype="multipart/form-data">
            @endif
                @csrf
                @if (isset($studio))
            <input type="hidden" name="sm_id" value="{{$studio->sm_id}}">
            <input type="hidden" name="am_id" value="{{$admin->am_id}}">
            <input type="hidden" name="old_logo" value="{{$logo}}">
                @endif
                <div class="form-group">
           <label>Studio Name</label>
        </div>
             <div class="col bg-dark">
                <h1 class="text-white">Admin Details</h1>
             </div>
             <div class="form-group">
                <label for="exampleFormControlTextarea1">Admin Profile Picture </label><br>
                @if (isset($logo))
                    
                <div>
                    <img src="{{asset($logo)}}" class=".img-rounded" style="width: 150px;" alt="">
                </div>
                @endif
                <input type="file" name="fileupload" id="fileupload" class="form-control" accept="image/*"  />
                <small id="formHelp" class="form-text text-danger ">
                    @error( "fileupload" )
                        {{ ucwords($message) }}
                    @enderror
               </small>
              </div>
             <div class="form-group">
                <label>Full Name</label>
                <input type="text" name="studio_admin_name" id="studio_admin_name" class="form-control"  placeholder="Enter Full Name" value="{{old('studio_admin_name')}}{{ $admin->am_name??'' }}"/>
                <small id="formHelp" class="form-text text-danger ">
                    @error( "studio_admin_name" )
                        {{ ucwords($message) }}
                    @enderror
               </small>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="studio_admin_email" id="studio_admin_email" class="form-control"  placeholder="Enter Email Address" value="{{old('studio_admin_email')}}{{ $admin->am_email??'' }}"/>
                <small id="formHelp" class="form-text text-danger ">
                    @error( "studio_admin_email" )
                        {{ ucwords($message) }}
                    @enderror
               </small>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="studio_admin_password" id="studio_admin_password" class="form-control"  placeholder="Enter Password" value="{{old('studio_admin_password')}}"/>
                <small id="formHelp" class="form-text text-danger ">
                    @error( "studio_admin_password" )
                        {{ ucwords($message) }}
                    @enderror
               </small>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="studio_admin_cnf_password" id="studio_admin_cnf_password" class="form-control"  placeholder="Enter Confirm Password" value="{{old('studio_admin_cnf_password')}}"/>
                <small id="formHelp" class="form-text text-danger ">
                    @error( "studio_admin_cnf_password" )
                        {{ ucwords($message) }}
                    @enderror
               </small>
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-success mr-2"><span id="studio_add_btn_text">Submit</span> <span id="loader" class="d-none"><i class="fa fa-spinner fa-spin "></i>Submiting</span></button>
                <button type="reset" class="btn btn-secondary" id="resetForm">Cancel</button>
            </div>
        </form>
          </div>
         <div class="card-footer">
           
         </div>
     
        <!--end::Form-->
       </div>
</div>
   
   
    

@endsection
@section('scripts')
<script src="{{asset('js/custom/admin.js')}}"></script>
@endsection