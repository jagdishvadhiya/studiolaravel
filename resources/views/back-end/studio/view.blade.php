@extends('back-end.layouts.master')
@section('subHeader')
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Studio</span>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">View</span>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
@endsection

@section('content')

    
    <div class="col-12">
        <div class="col-xxl-8 order-2 order-xxl-1 pb-5">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0 text-center">
                    <div class="card-title ">
                        <h3 class="card-label">Studio Details </h3>
                    </div>
                   
                </div>
                <div class="card-body">
                   
                    
                </div>
            </div>
        </div>
           
    </div>
   
   
    

@endsection
@section('styles')
<script src="{{asset('css/custom/addStudio.css')}}"></script>
@endsection
@section('scripts')
{{-- <script src="{{asset('js/pages/crud/file-upload/dropzonejs.js?v=7.0.9')}}"></script> --}}
<script src="{{asset('js/custom/studio.js')}}"></script>
@endsection