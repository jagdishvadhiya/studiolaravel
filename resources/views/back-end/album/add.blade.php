@extends('back-end.layouts.master')
@section('subHeader')
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Studio</span>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Albums</span>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Add New</span>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
        </div>
    </div>
@endsection

@section('content')

    
    <div class="col-12">
        <div class="card card-custom">
            <div class="card-header">
             <h3 class="card-title">
              @if  (isset($studio))
                  Edit Studio
              @else
             Add New Album
             @endif
             </h3>
            </div>
            <!--begin::Form-->
           
               
             <div class="card-body">
                
                 @if  (isset($studio))
                  <form class="form" id="studioAlbumFormUpdate" name="studioAlbumFormUpdate" method="post" action="{{route('backend.studio.album.addForm')}}" enctype="multipart/form-data">
                @else
                <form class="form" id="studioAlbumForm" name="studioAlbumForm" method="post" action="{{route('backend.studio.album.addForm')}}" enctype="multipart/form-data">
                @endif
                    @csrf
                    @if (isset($studio))
                <input type="hidden" name="sm_id" value="{{$studio->sm_id}}">
                <input type="hidden" name="am_id" value="{{$admin->am_id}}">
                <input type="hidden" name="old_logo" value="{{$logo}}">
                    @endif
                    @if ( isset(session('login')['studio_id']) )
                    <input type="hidden" name="studio_id" value="{{ session("login")['studio_id'] }}">
                    @endif
                 <div class="col bg-dark">
                    <h1 class="text-white">Album Details</h1>
                 </div>
                 @if (session('login')['role'] == "superadmin" )
                 <div class="form-group">
                    <label>Select Studio</label>
                    <select name="studio_id" id="studio" class="form-control">
                        <option value="">Select Studio</option>
                    </select>
                    <small id="formHelp" class="form-text text-danger ">
                        @error( "studio_album_name" )
                            {{ ucwords($message) }}
                        @enderror
                   </small>
                </div>
                @endif
                 <div class="form-group">
                    <label>Album Name</label>
                    <input type="text" name="studio_album_name" id="studio_album_name" class="form-control"  placeholder="Enter Full Name" value="{{old('studio_album_name')}}"/>
                    <small id="formHelp" class="form-text text-danger ">
                        @error( "studio_album_name" )
                            {{ ucwords($message) }}
                        @enderror
                   </small>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Album Discription</label>
                    <textarea class="form-control" name="studio_album_discription" id="studio_album_discription" rows="3" placeholder="Album Discription">{{old('studio_album_discription')}}</textarea>
                    <small id="formHelp" class="form-text text-danger ">
                        @error( "studio_album_discription" )
                            {{ ucwords($message) }}
                        @enderror
                   </small>
                </div>
             
               <div class="col">
                   <div class="form-group">
                    <label for="text">Album Images</label>
                   </div>
               </div>
                <div id="myDropzon" class="col my-2 fallback ">
                    <div class="col-lg-12 col-md-9 col-sm-12">
                        <div class="dropzone dropzone-default dropzone-success dz-clickable" id="myDropzone">
                            <div class="dropzone-msg dz-message needsclick">
                                <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                <span class="dropzone-msg-desc">Only Images Are Allowed For Upload</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group px-4">
                    <button type="button" id="upload_images" class="btn btn-block btn-success "><span id="studio_album_upload_text">Upload Images</span> <span id="upload_loader" class="d-none"><i class="fa fa-spinner fa-spin "></i>Uploading</span></button>
                </div>
               
                <div class="row d-none" id="showImages">
                    <div class="col-12 bg-dark text-white my-2">    
                        <h3 class="text-center">Uploaded Images</h3>
                    </div>
                    <div class="row border border-dark justify-content-left m-2 p-2 " id="imagesBox">   
                    </div>
                </div> 
                   
                <div class="form-group">
                <div class="row">    
                    <div class="col-6">

                        <button type="submit" class="btn btn-success mr-2 btn-block"><span id="studio_album_add_btn_text">Submit</span> <span id="loader" class="d-none"><i class="fa fa-spinner fa-spin "></i>Submiting...</span></button>
                    </div>
                    <div class="col">

                        <button type="reset" class="btn btn-secondary btn-block" id="resetForm">Reset</button>
                    </div>
                    </div>
                </div>
            </form>
           
              </div>
             <div class="card-footer">
                {{-- <button type="submit" class="btn btn-success mr-2"><span id="studio_add_btn_text">Submit</span> <span id="loader" class="d-none"><i class="fa fa-spinner fa-spin "></i>Submiting</span></button>
                <button type="reset" class="btn btn-secondary">Cancel</button> --}}
             </div>
         
            <!--end::Form-->
           </div>
    </div>
   
   
    

@endsection
@section('styles')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /   >
{{-- <script src="{{asset('css/custom/album.css')}}"></script> --}}
<style>
    #myDropzone{
    /* width: 200px ! important;
    height: 200px; */
    background-color: aquamarine;
}
#imagesBox{
    width: 100%;
}




</style>
@endsection
@section('scripts')
<script>
   var ablumListUrl = "{{route('backend.studio.album')}}";
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script src="{{asset('js/pages/crud/file-upload/dropzonejs.js?v=7.0.9')}}"></script>
<script src="{{asset('js/custom/album.js')}}"></script>
@endsection