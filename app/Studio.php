<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Studio extends Model
{
    protected $table = "studio_master";
    protected $guarded = ['*'];
    protected $primaryKey = 'sm_id';
    public $timestamps = false;
    protected $fillable = [
        'sm_name',"sm_description","sm_logo_image_id","sm_status","sm_admin_id"
    ];
}
