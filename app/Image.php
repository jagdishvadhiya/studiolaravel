<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "image_master";
    public $timestamps = FALSE;
    protected $primaryKey = 'im_id';
    protected $fillable = [
        'im_name','im_type','im_album_id','im_order'
    ];
    protected $guarded = ['*'];
}
