<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "role_master";
    protected $guarded = ['*'];
    protected $primaryKey = 'rm_id';
    public $timestamps = false;
}
