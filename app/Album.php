<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = "album_master";
    public $timestamps = FALSE;
    protected $primaryKey = 'alm_id';
    protected $fillable = [
        'alm_name','alm_link','alm_studio_id','alm_description'
    ];
    protected $guarded = ['*'];
}
