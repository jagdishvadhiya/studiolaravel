<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "admin_master";
    public $timestamps = FALSE;
    protected $primaryKey = 'am_id';
    protected $fillable = [
        'am_name', 'am_email', 'am_password',
    ];
    protected $guarded = ['_token','um_cnf_password'];
}
