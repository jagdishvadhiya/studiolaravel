<?php

namespace App\Http\Controllers;

use App\Studio;
use Illuminate\Http\Request;

class genralController extends Controller
{
    // get studio id and name list
    public function getStudioIdName(Request $request)
    {
        if($request->ajax())
        {
            if(session("login")['role'] == "superadmin" )
            {
                $studioIdName = Studio::get(['sm_id','sm_name'])->toArray();
                return response()->json($studioIdName);
            }
        }

    }
}
