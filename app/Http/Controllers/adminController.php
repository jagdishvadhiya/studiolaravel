<?php

namespace App\Http\Controllers;

use App\Image;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Throwable;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class adminController extends Controller
{
    public function index(Request $request)
    {
  
        if ($request->ajax()) {
            $sm_id = $request->studio_id;
            $data = DB::table('admin_master')
                ->join('studio_master', 'studio_master.sm_admin_id', '=', 'admin_master.am_id')
                ->where("studio_master.sm_id",$sm_id)
                ->select('admin_master.*')
                ->get();
            // $data = User::where("sm_admin_id",$sm_id);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                    if($row->am_status == "active")
                    {
                        $class = 'badge-success';
                    }else{
                        $class = 'badge-danger';

                    }
                    $btn = '<p  class="badge '.$class.' ">'.$row->am_status.'</p> ';
                    return $btn;
                })
                ->addColumn('avatar', function($row){
                    // $image = '/images/user/'.Image::find($row->am_image_id)->im_name;
                    // $btn = '<img src="'.asset($image).'" style="width: 70px"/> ';
                    $btn = '<img src="" style="width: 70px"/> ';
                    return $btn;
                })
                ->addColumn('role', function($row){
                    $admin = Role::find($row->am_role_id)->rm_type;
                    return $admin;
                })
                ->addColumn('action', function($row){

                    $btn = '<a href="'.route('backend.studio.admin.edit',['id'=>$row->am_id]).'" class="edit btn btn-success btn-sm">Edit</a> <button data-link="'.route('backend.studio.admin.delete',['id'=>$row->am_id]).'"  id="'.$row->am_id.'" class="delete btn btn-danger btn-sm ">Delete</button>';
                    return $btn;
                })
                ->addColumn('checkbox',function($row){
                return '<input type="checkbox" name="studio_checkbox[]" class="studio_checkbox form-control-sm" value="'.$row->am_id.'" id="singleCheck">';
                })
                ->rawColumns(['status','action','avatar','role','checkbox'])
                ->make(true);
        }
        return view('back-end.admin.index');
    }
    public function add()
    {
        return view('back-end.admin.add');
    }
    public function addForm(Request $request)
    {

       dd("ad form");
       $response = array('status'=>"false","message"=>"somthing Wrong");
       if($request->ajax())
       {
           
            $rules=[
                       
                        "studio_admin_name"=> "required",
                        "studio_admin_password"=> "required",
                        'fileupload'=>"mimetypes:image/*",
                        "studio_admin_email"=> [
                            "required",
                            "email"
                        ],
                        "studio_admin_cnf_password"=>[
                            "required",
                            "same:studio_admin_password"
                        ]
            ];
            $msg = [
                "required"=>'Plese Fill :attribute ',
                'email'=>"email Invalid",
                "same"=>'password NOt Match',
                "fileupload"=>[ 
                    
                    "mimetypes"=>"Please Upload Only Image",
                ]
            ];
            $validate = Validator::make($request->input(),$rules,$msg);
            if($validate->fails())
            {
                $response['errors']=$validate->errors();
                if($validate->errors()->messages()['fileupload'][0] != "")
                {
                    $response['message']=$validate->errors()->messages()['fileupload'][0];
                }
            }else{
                
                try {
                    $file = $request->file('fileupload');
                    $name = rand().$file->getClientOriginalName();
                    $file->move(public_path().'/images/user/', $name);
                    $image_data=['im_name'=>$name];
                    $image = new Image();
                    $image_inserted = $image->create($image_data);
                    $role_id=Role::where("rm_type","admin")->get(["rm_id"])->first();
                    $admin_create=[
                        "am_name"=>$request->input("studio_admin_name"),
                        "am_email"=>$request->input("studio_admin_email"),
                        "am_password"=>md5($request->input("studio_admin_password")),
                        "am_role_id"=>$role_id['rm_id'],
                    ];
                    $admin = new User();
                    $admin_inserted = $admin->create($admin_create);
                    $studio_create=[
                        "sm_name"=>$request->input("studio_name"),
                        "sm_description"=>$request->input("studio_discription"),
                        "sm_logo_image_id"=>$image_inserted->im_id,
                        "sm_admin_id"=>$admin_inserted->am_id,
                    ];
                    $studio = new Studio();
                    if($studio->create($studio_create))
                    {
                        $response['status']="true";
                        $response['message']="Studio Created Successfully...";
                    }
                } catch (Throwable $e) {
                    $response['errors']=$e->getMessage();
                }
                
                $response['file']=$name;
            }
       }
    
    return response()->json($response);
    }
    public function edit($id)
    {
        return view('back-end.admin.add');
    }
    public function delete($id)
    {
        dd($id);
    }
}
