<?php

namespace App\Http\Controllers;

use App\Album;
use App\Image;
use Facade\FlareClient\Http\Response;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Throwable;
use Intervention\Image\Facades\Image as ImageIntervation;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class albumController extends Controller
{
    public function index(Request $request)
    {
       // dattable processing
       if ($request->ajax()) {
        $sm_id = $request->studio_id;
        $data = Album::where("alm_studio_id",$sm_id);
        // $data = User::where("sm_admin_id",$sm_id);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function($row){
                if($row->alm_status == "active")
                {
                    $class = 'badge-success';
                }else{
                    $class = 'badge-danger';

                }
                $btn = '<p  class="badge '.$class.' ">'.$row->alm_status.'</p> ';
                return $btn;
            })
            ->addColumn('isprotected', function($row){
                if($row->alm_password === null)
                {
                    $icon = '<i class="fa fa-times text-danger " aria-hidden="true"></i>';
                }else{
                    $icon = '<i class="fa fa-check text-success " aria-hidden="true"></i>';

                }
                $btn = '<p  class="h3 text-center ">'.$icon.'</p> ';
                return $btn;
            })
            ->addColumn('images', function($row){
                $btn = '<span id="viewImages" data-albumid="'.$row->alm_id.'"><i class="fa fa-eye text-success h3" aria-hidden="true"></i></span>';
                return $btn;
            })
            ->addColumn('action', function($row){

                $btn = '<a href="'.route('backend.studio.album.edit',['id'=>$row->alm_id]).'" class="edit btn btn-success btn-sm">Edit</a> <button data-link="'.route('backend.studio.album.delete',['id'=>$row->alm_id]).'"  id="'.$row->alm_id.'" class="delete btn btn-danger btn-sm ">Delete</button>';
                return $btn;
            })
            ->addColumn('checkbox',function($row){
            return '<input type="checkbox" name="studio_checkbox[]" class="studio_checkbox form-control-sm" value="'.$row->alm_id.'" id="singleCheck">';
            })
            ->rawColumns(['status','action','images','isprotected','checkbox'])
            ->make(true);
    }
        return view('back-end.album.index');
    }
    public function add()
    {
        $albumTempStoreFolder = \public_path('/images/album/temp/');
        $files = scandir($albumTempStoreFolder);               
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {  
                    $admin_id_image = explode("_",$file)[0]; 
                    $admin_id = session("login")['adminId'];
                    if( $admin_id == $admin_id_image )
                    {

                        unlink($albumTempStoreFolder.$file);
                    }
                }
            }
        }
        return view('back-end.album.add');
    }
    public function edit($id)
    {
        return view('back-end.album.edit');
    }

    public function delete(Request $request)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
        $images_data = Image::where('im_album_id',$request->id)->get();
        $images_name = [];
        $images_ids = [];
        foreach($images_data as $image)
        {
            $images_ids[] = $image["im_id"];
            $images_name[] = $image["im_name"];
        }
        $albumImagePath=\public_path("/images/album/");
        $albumImageThumbnailPath=\public_path("/images/album/thumbnail/");
        foreach($images_name as $image)
        {
            unlink($albumImagePath.$image);
            unlink($albumImageThumbnailPath.$image);
        }
        if(Album::where("alm_id",$request->id)->delete())
        {
            if(Image::where("im_album_id",$request->id)->delete())
            {
                $response['status']="true";
                $response['message']="Album Deleted Successfully...";
            }else{
                $response['message']="Album Not Deleted Somthing Is Wrong... ";
            }
        }else{
            $response['message']="Album Not Deleted Somthing Is Wrong... ";
        }
       
       
        return response()->json($response);
    }
    public function fetchImages(Request $request)
    {
        if($request->method() != "GET")
        {
            $image_data = Image::where("im_album_id",$request->alb_id)->get("im_name");
            $temp_images  = array();
            foreach($image_data as $image)
            {
                $obj['name'] =$image->im_name;
                $temp_images[] = $obj;
            }
            return response()->json($temp_images);

        }
        $storeFolder = \public_path('/images/album/temp/');
        $temp_images  = array();
        
        $files = scandir($storeFolder);               
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {  
                    $admin_id_image = explode("_",$file)[0]; 
                    $admin_id = session("login")['adminId'];
                    if( $admin_id == $admin_id_image )
                    {

                        $obj['name'] = $file;
                        $obj['size'] = filesize($storeFolder.$file);
                        $temp_images[] = $obj;
                    }
                }
            }
        }
        return response()->json($temp_images);
    }
    public function deleteImages(Request $request)
    {
        if($request->ajax())
        {
            $imageName = $request->imagename;
            $imageType = $request->imagetype;
            if($imageType != "album")
            {
                $path=\public_path("/images/album/temp/".$imageName);
            }else{
                $path=\public_path("/images/album/".$imageName);
            }
            
            if(unlink($path))
            {
                return response()->json('true');
            }else{
                return response()->json('false');
            }
        }
        
    }
    public function addForm(Request $request)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
        $rules=[
            "studio_album_name"=> "required",
            "studio_album_discription"=> "required",
        ];
        $msg = [
            "required"=>'Plese Fill :attribute ',
        ];
        if($request->ajax())
        {
            
            $validate = Validator::make($request->input(),$rules,$msg);
            if($validate->fails())
            {
                $response['errors']=$validate->errors();
                $response['message']="Input Field Error";
                
            }else{
                if($request->albumimages)
                {
                    // dd($request->all());
                    $unique_link = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10); 
                    $album_data = [
                        'alm_studio_id' => $request->studio_id,
                        'alm_name' => $request->studio_album_name,
                        'alm_description' => $request->studio_album_discription,
                        "alm_link" => $unique_link, 
                    ];
                    $album=new Album();
                    $album_id = $album->create($album_data)->alm_id;
                    $imagesArray = explode(",",$request->albumimages);
                    $image_data = [];
                    $no=1;
                    foreach($imagesArray as $image)
                    {
                        $one_image_data = [
                            "im_type"=>"album",
                            "im_name"=>$image,
                            "im_album_id"=>$album_id,
                            "im_order"=>$no,
                        ];
                        $image_data[]=$one_image_data;
                        $no++;
                    }
                   if(Image::insert($image_data))
                   {
                        if($album_id)
                        {
                            $albumImageTempPath=\public_path("/images/album/temp/");
                            $albumImagePath=\public_path("/images/album/");
                            $albumImageThumbnailPath=\public_path("/images/album/thumbnail/");
                            foreach($imagesArray as $image)
                            {
                                $img = ImageIntervation::make($albumImageTempPath.$image);
                                $img->resize(400, 400);
                                $img->save($albumImageThumbnailPath.$image);
                                File::move($albumImageTempPath.$image,$albumImagePath.$image);
                            }
                            $response['status']="true";
                            $response['message']="Album Created Successfully ...";
                        }
                   }else{
                    $response['message']="Somthing Wrong In Image Upload";
                   }
                    // dd($image_data);
                }else{
                    $response['message']="No Images ! Please choose Atlist One Image";
                }
            }
        }else{
            $request->validate($rules,$msg);
        }
       return response()->json($response);
    }
    public function dropzone(Request $request)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
        if($request->file('file'))
        {
            // dd($request->all());
            try {
                $file = $request->file('file');
                $admin_id = session("login")['adminId'];
                $name = $admin_id."_album_".substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10)."_".$file->getClientOriginalName();
                $file->move(public_path().'/images/album/temp/', $name);
                $response['status']="true";
                $response['message']="Image Uploade Successfully..";
            } catch (Throwable $e) {
                $response['errors']=$e->getMessage();
            }
        }
            return response()->json($response);
    }
}
