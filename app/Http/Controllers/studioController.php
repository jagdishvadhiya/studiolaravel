<?php

namespace App\Http\Controllers;

use App\Image;
use App\Role;
use App\Studio;
use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidationValidator;
use Illuminate\Support\Facades\File; 
use Throwable;
use Yajra\DataTables\DataTables;

class studioController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Studio::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                    if($row->sm_status == "active")
                    {
                        $class = 'badge-success';
                    }else{
                        $class = 'badge-danger';

                    }
                    $btn = '<p  class="badge '.$class.' ">'.$row->sm_status.'</p> ';
                    return $btn;
                })
                ->addColumn('logo', function($row){
                    $logo = '/images/studio/'.Image::find($row->sm_logo_image_id)->im_name;
                    $btn = '<img src="'.asset($logo).'" style="width: 70px"/> ';
                    return $btn;
                })
                ->addColumn('admin', function($row){
                    $admin = User::find($row->sm_admin_id)['am_name'];
                    return $admin;
                })
                ->addColumn('action', function($row){

                    $btn = '<a href="'.route('backend.studio.edit',['id'=>$row->sm_id]).'" class="edit btn btn-success btn-sm">Edit</a> <button data-link="'.route('backend.studio.delete',['id'=>$row->sm_id]).'"  id="'.$row->sm_id.'" class="delete btn btn-danger btn-sm ">Delete</button>';
                    return $btn;
                })
                ->addColumn('checkbox',function($row){
                return '<input type="checkbox" name="studio_checkbox[]" class="studio_checkbox form-control-sm" value="'.$row->sm_id.'" id="singleCheck">';
                })
                ->rawColumns(['status','action','logo','admin','checkbox'])
                ->make(true);
        }
        return view('back-end.studio.index');
    }
    public function add()
    {
            return view('back-end.studio.add');
        
    }
    public function edit($id = "")
    {
        if($id)
        {
            $studio = Studio::find($id);
            $logo = '/images/studio/'.Image::find($studio->sm_logo_image_id)->im_name;
            $admin = User::find($studio->sm_admin_id);
            return view('back-end.studio.add',compact('studio','logo','admin'));
        }else{
            return redirect(route('back-end.studio'));
        }
    }
    public function addForm(Request $request)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
       if($request->ajax())
       {
           
            $rules=[
                        "studio_name"=> "required",
                        "studio_discription"=> "required",
                        "studio_admin_name"=> "required",
                        "studio_admin_password"=> "required",
                        // 'fileupload'=>"required|mimetypes:image/*",
                        "studio_admin_email"=> [
                            "required",
                            "email"
                        ],
                        "studio_admin_cnf_password"=>[
                            "required",
                            "same:studio_admin_password"
                        ]
            ];
            $msg = [
                "required"=>'Plese Fill :attribute ',
                'email'=>"email Invalid",
                "same"=>'password NOt Match'
            ];
            $validate = Validator::make($request->input(),$rules,$msg);
            if($validate->fails())
            {
                $response['errors']=$validate->errors();
            }else{
                
                try {
                    $file = $request->file('fileupload');
                    $name = rand().$file->getClientOriginalName();
                    $file->move(public_path().'/images/studio/', $name);
                    $image_data=[
                        'im_name'=>$name,
                        'im_type'=>'studio',
                    ];
                    $image = new Image();
                    $image_inserted = $image->create($image_data);
                    $role_id=Role::where("rm_type","admin")->get(["rm_id"])->first();
                    $admin_create=[
                        "am_name"=>$request->input("studio_admin_name"),
                        "am_email"=>$request->input("studio_admin_email"),
                        "am_password"=>md5($request->input("studio_admin_password")),
                        "am_role_id"=>$role_id['rm_id'],
                    ];
                    $admin = new User();
                    $admin_inserted = $admin->create($admin_create);
                    $studio_create=[
                        "sm_name"=>$request->input("studio_name"),
                        "sm_description"=>$request->input("studio_discription"),
                        "sm_logo_image_id"=>$image_inserted->im_id,
                        "sm_admin_id"=>$admin_inserted->am_id,
                    ];
                    $studio = new Studio();
                    if($studio_data = $studio->create($studio_create))
                    {
                        $studio_id = $studio_data->sm_id;
                        $admin_studio_data = User::find($admin_inserted->am_id);
                        $admin_studio_data->am_studio_id=$studio_id;
                        $result=$admin_studio_data->save();
                        if($result == 1)
                        {

                            $response['status']="true";
                            $response['message']="Studio Created Successfully...";
                        }
                    }
                } catch (Throwable $e) {
                    $response['errors']=$e->getMessage();
                }
                
                $response['file']=$name;
            }
       }
    
    return response()->json($response);
    }
    public function editForm(Request $request)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
        if($request->ajax())
        {
            $rules=[
                "studio_name"=> "required",
                "studio_discription"=> "required",
                "studio_admin_name"=> "required",
                
                "studio_admin_email"=> [
                    "required",
                    "email"
                ],
                "studio_admin_cnf_password"=>[
                    
                    "same:studio_admin_password"
                ]
            ];
            $msg = [
                "required"=>'Plese Fill :attribute ',
                'email'=>"email Invalid",
                "same"=>'password NOt Match'
            ];
            if($request->fileupload)
            {
                $rules["fileupload"]="mimetypes:image/*";
                $msg['mimetypes']='Please Upload Image Only';
            }
            $validate = Validator::make($request->input(),$rules,$msg);
            if($validate->fails())
            {
                $response['errors']=$validate->errors();
                if($validate->errors()->messages()['fileupload'][0] != "")
                {
                    $response['message']=$validate->errors()->messages()['fileupload'][0];
                }
                
            }else{
                
                try {
                   
                    $old_image = $request->old_logo;
                    $old_image_name = pathinfo($old_image)['basename'];
                    if($request->fileupload)
                    {
                        if(File::exists(\public_path($old_image))) {
                            File::delete(\public_path($old_image));
                        }
                        DB::table('image_master')->where('im_name', $old_image_name)->delete();
                        $file = $request->file('fileupload');
                        $name = rand().$file->getClientOriginalName();
                        $file->move(public_path().'/images/studio/', $name);
                        $image_data=['im_name'=>$name];
                        $image = new Image();
                        $image_id = $image->create($image_data)->im_id;
                        $response['status']="true";
                        $response['message']="Studio Created Successfully...";
                    }else{
                        $response['status']="true";
                        $response['message']="Studio Created Successfully...";
                        $image_id = Image::where('im_name',$old_image_name)->get(['im_id'])->first()->im_id;
                    }
                    $role_id=Role::where("rm_type","admin")->get(["rm_id"])->first();
                    $admin_create=[
                        "am_name"=>$request->input("studio_admin_name"),
                        "am_email"=>$request->input("studio_admin_email"),
                        "am_role_id"=>$role_id['rm_id'],
                    ];
                    if($request->studio_admin_password)
                    {
                        $admin_create["am_password"]=md5($request->input("studio_admin_password"));
                    }
                    $admin_update = User::where("am_id",$request->am_id)->update($admin_create);
                    $studio_create=[
                        "sm_name"=>$request->input("studio_name"),
                        "sm_description"=>$request->input("studio_discription"),
                        "sm_logo_image_id"=>$image_id,
                        "sm_admin_id"=>$request->am_id,
                        "sm_status"=>$request->input('studio_status'),
                    ];
                    $studio_update = Studio::where("sm_id",$request->sm_id)->update($studio_create);
                    // dd($studio_update,$admin_update);
                    if( $studio_update == 1 || $admin_update == 1 )
                    {
                        $response['status']="true";
                        $response['message']="Studio Created Successfully...";
                    }else{
                        $response['status']="false";
                        $response['message']="Studio Not Updated Successfully...";   
                    }
                } catch (Throwable $e) {
                    $response['errors']=$e->getMessage();
                }
                
                // $response['file']=$name;
            }
        }
        return response()->json($response);
    }
    public function delete(Request $request,$id)
    {
        $response = array('status'=>"false","message"=>"somthing Wrong");
        try{
            $studio = Studio::find($id);
            $image_id = $studio->sm_logo_image_id;
            $admin_id = $studio->sm_admin_id;
            $image_name = Image::find($image_id)->im_name;
            if(File::exists(\public_path('/images/user/'.$image_name))) {
                File::delete(\public_path('/images/user/'.$image_name));
            }
            User::find($admin_id)->delete();
            DB::table('image_master')->where('im_name', $image_name)->delete();
            $studio->delete();
            $response['status']="true";
            $response['message']="Studio Deleted Successfully...";
        }catch (Throwable $e) {
            $response['message']=$e->getMessage();
        }
       
        return response()->json($response);
    }
    public function view()
    {
        return view("back-end.studio.view");
    }
}
