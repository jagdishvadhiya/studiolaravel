<?php

namespace App\Http\Controllers;

use App\Role;
use App\Studio;
use App\User;
use Dotenv\Validator;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class loginController extends Controller
{
    public function index()
    {
        return view('back-end.login');
    }
    public function login(Request $request)
    {
        $response = array("status"=>"false","message"=>"Somthing Wrong...");
        if($request->ajax())
        {   
            $rules=[
                'am_email'=>'required',
                'am_password'=>'required',
            ];
            $messages=[
                "required"=>"The :attribute field is required",
                "am_email.unique"=>"This Email Is already taken By Someone Else",
            ];
            $validate = FacadesValidator::make($request->all(),$rules,$messages);
            if($validate->fails())
            {
                $response['errors']=$validate->errors();
            }else{
                $data = User::where("am_email",$request->input('am_email'))->get(['am_id',"am_email","am_password","am_name","am_role_id"])->first();
                if(!empty($data))
                {
                    if($data->am_password == md5($request->input('am_password')))
                    {
                        $role = Role::where("rm_id",$data->am_role_id)->get(["rm_type"])->first();
                        $session = [
                                    'adminId'=>$data->am_id,
                                    'name'=>$data->am_name,
                                    'email'=>$data->am_email,
                                    'role'=>$role->rm_type,
                                    'user_id'=>$data->am_id
                                    ];
                                   
                        if($role->rm_type != "superadmin" )
                        {
                            $session['studio_id']=Studio::where("sm_admin_id",$data->am_id)->get(["sm_id"])->first()->sm_id;
                        }
                       $request->session()->put( "login" , $session );
                       $response['status']='true';
                       $response['message']='Login SuccessFull...';
                    }else{
                        $response['message']='Password Not Matched';
                    }
                }else{
                    $response['message']='Email Not Exist In Our Records !!!';

                }
            }
            return response()->json($response);
        }
    }
    public function logout(Request $request)
    {
        $request->session()->forget('login');
        return redirect()->back();
    }
}
