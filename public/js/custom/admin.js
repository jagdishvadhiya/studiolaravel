$(document).ready(function(){
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    $("span#loader").hide();
    // validate form for add new record
    $("#studioAdminForm").validate({
                    rules: {
                                
                                studio_admin_name: "required",
                                studio_admin_password: "required",
                                fileupload:{
                                    required: true,
                                },
                                studio_admin_email: {
                                    required: true,
                                    email: true
                                },
                                studio_admin_cnf_password:{
                                    required: true,
                                    equalTo: "#studio_admin_password"
                                }
                            
                            },
                    messages: {
                                
                                studio_admin_name: "Please enter Admin FullName",
                                studio_admin_email: {
                                                        required:"Please Enter Email Address",
                                                        email:"Email Is not valid",
                                                        filesize:"File is not Morethen 3 Mb"
                                                    },
                                fileupload: {
                                                        required:"Please Select Profile Picture",
                                                        
                                                    },
                                studio_admin_password: "Please enter Password",
                                studio_admin_cnf_password:{
                                                                required:"Please Enter Password Again",
                                                                equalTo:"Confirm Password Is not Matched With Password"
                                                            },
                        
                            },
                    errorElement : 'small',
                    errorClass: 'form-text text-danger emailError text-capitalize',
                   
                  });
    //form submit
    $("#studioAdminForm").on('submit', function(e) {
    var isvalid = $("#studioAdminForm").valid();
    if (isvalid) {
        e.preventDefault();
        var fileInput = document.getElementById('fileupload');
        var file = fileInput.files[0];
        var formData = new FormData($('form')[0]);
        formData.append('file', file);
        formData.append('_token', CSRF_TOKEN);
        var url =$(this).attr("action");
        var type = $(this).attr("method");
        $.ajax({
                    url:url,
                    type:type,
                    data:formData,
                    processData: false,
                    contentType: false,
                    success:function(response)
                    {
                        if(response.status == "true")
                        {
                            swal({
                                text:response.message,
                                icon:"success"
                            });
                            $("button#resetForm").click();
                        }else{
                            swal({
                                text:response.message,
                                icon:"error"
                            }); 
                        }
                    }
                });     
    }else{
        e.preventDefault();
        console.log(isvalid);
    }
    });
 // validate form for add new record
 $("#studioAdminFormUpdate").validate({
    rules: {
                studio_name: "required",
                studio_discription: "required",
                studio_admin_name: "required",
                studio_admin_email: {
                    required: true,
                    email: true
                },
                studio_admin_cnf_password:{
                    equalTo: "#studio_admin_password"
                }
            
            },
    messages: {
                studio_name: "Please enter your Studio Name",
                studio_discription: "Please enter Studio Discription",
                studio_admin_name: "Please enter Admin FullName",
                studio_admin_email: {
                                        required:"Please Enter Email Address",
                                        email:"Email Is not valid"
                                    },
                studio_admin_password: "Please enter Password",
                studio_admin_cnf_password:{
                                                equalTo:"Confirm Password Is not Matched With Password"
                                            },
        
            },
    errorElement : 'small',
    errorClass: 'form-text text-danger emailError text-capitalize',
   
  });
//form submit
$("#studioAdminFormUpdate").on('submit', function(e) {
var isvalid = $("#studioAdminFormUpdate").valid();
if (isvalid) {
e.preventDefault();
var filesupload = $("input#fileupload");
var fileInput = document.getElementById('fileupload');
var file = fileInput.files[0];
var formData = new FormData($('form')[0]);
formData.append('file', file);
formData.append('_token', CSRF_TOKEN);
var url =$(this).attr("action");
// alert(url);
var type = $(this).attr("method");
$.ajax({
    url:url,
    type:type,
    data:formData,
    processData: false,
    contentType: false,
    success:function(response)
    {
        if(response.status == "true")
        {
            swal({
                text:response.message,
                icon:"success"
            });
            location.reload();
        }else{
            swal({
                text:response.message,
                icon:"error"
            }); 
        }
    }
});     
}
});
var currunt_url = window.location.href;
if(currunt_url.includes('dashboard'))
{
    currunt_url=currunt_url.replace('dashboard','studio/admin');
}
var sm_id = '9';

    // datatable
    var table = $('#adminDatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: currunt_url+"/datatable",
            type:"POST",
            data:{
                "_token":CSRF_TOKEN,
                "studio_id":sm_id
            }
        },
        columns: [
            { data:"checkbox", name:"checkbox", orderable:false, searchable:false},
            {data: 'am_id', name: 'am_id'},
            {data: 'am_name', name:'am_name'},
            {data: 'am_email', name: 'am_email'},
            {
                data: 'avatar',
                name: 'avatar',
                orderable: false,
                searchable: false
            },
            {data: 'role', name: 'role'},
            {data: 'status', name: 'status'},
            {data: 'am_created_at', name: 'am_ceated_at'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "preDrawCallback": function( settings ) {
            // $('.student_checkbox:checked').each(function(){
            //     id.push($(this).val());
            // });
          },
        "drawCallback": function( row, data, index ) {
            // var check = $("input:checkbox#checkAll").is(":checked")?true:false;
            // $("input:checkbox").prop('checked', check);
        }

    });
  
// delete button click
$(document).on("click","button.delete",function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Studio",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            var url =$(this).data("link");
    $.ajax({
        url:url,
        type:"POST",
        data:{
            "_token":CSRF_TOKEN
        },
        success:function(response){
            if(response.status == "true")
            {
                swal({
                    text:response.message,
                    icon:"success"
                });
            }else{
                swal({
                    text:response.message,
                    icon:"error"
                });
            }
            table.ajax.reload();
        }
    });

        } else {
          swal("Studio is safe!");
        }
      });
    
});
    
});