$(document).ready(function(){
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    $("span#loader").hide();
    // validate form for add new record
    $("#studioFrom").validate({
                    rules: {
                                studio_name: "required",
                                studio_discription: "required",
                                studio_admin_name: "required",
                                studio_admin_password: "required",
                                fileupload:"required",
                                studio_admin_email: {
                                    required: true,
                                    email: true
                                },
                                studio_admin_cnf_password:{
                                    required: true,
                                    equalTo: "#studio_admin_password"
                                }
                            
                            },
                    messages: {
                                studio_name: "Please enter your Studio Name",
                                studio_discription: "Please enter Studio Discription",
                                studio_admin_name: "Please enter Admin FullName",
                                studio_admin_email: {
                                                        required:"Please Enter Email Address",
                                                        email:"Email Is not valid"
                                                    },
                                studio_admin_password: "Please enter Password",
                                studio_admin_cnf_password:{
                                                                required:"Please Enter Password Again",
                                                                equalTo:"Confirm Password Is not Matched With Password"
                                                            },
                        
                            },
                    errorElement : 'small',
                    errorClass: 'form-text text-danger emailError text-capitalize',
                   
                  });
    //form submit
    $("#studioFrom").on('submit', function(e) {
    var isvalid = $("#studioFrom").valid();
    if (isvalid) {
        e.preventDefault();
        var filesupload = $("input#fileupload");
        var fileInput = document.getElementById('fileupload');
        var file = fileInput.files[0];
        var formData = new FormData($('form')[0]);
        formData.append('file', file);
        formData.append('_token', CSRF_TOKEN);
        var url =$(this).attr("action");
        // alert(url);
        var type = $(this).attr("method");
        $.ajax({
                    url:url,
                    type:type,
                    data:formData,
                    processData: false,
                    contentType: false,
                    success:function(response)
                    {
                        if(response.status == "true")
                        {
                            swal({
                                text:response.message,
                                icon:"success"
                            });
                            $("button#resetForm").click();
                        }else{
                            swal({
                                text:response.message,
                                icon:"error"
                            }); 
                        }
                    }
                });     
    }
    });
 // validate form for add new record
 $("#studioFromUpdate").validate({
    rules: {
                studio_name: "required",
                studio_discription: "required",
                studio_admin_name: "required",
                studio_admin_email: {
                    required: true,
                    email: true
                },
                studio_admin_cnf_password:{
                    equalTo: "#studio_admin_password"
                }
            
            },
    messages: {
                studio_name: "Please enter your Studio Name",
                studio_discription: "Please enter Studio Discription",
                studio_admin_name: "Please enter Admin FullName",
                studio_admin_email: {
                                        required:"Please Enter Email Address",
                                        email:"Email Is not valid"
                                    },
                studio_admin_password: "Please enter Password",
                studio_admin_cnf_password:{
                                                equalTo:"Confirm Password Is not Matched With Password"
                                            },
        
            },
    errorElement : 'small',
    errorClass: 'form-text text-danger emailError text-capitalize',
   
  });
//form submit
$("#studioFromUpdate").on('submit', function(e) {
var isvalid = $("#studioFromUpdate").valid();
if (isvalid) {
e.preventDefault();
var filesupload = $("input#fileupload");
var fileInput = document.getElementById('fileupload');
var file = fileInput.files[0];
var formData = new FormData($('form')[0]);
formData.append('file', file);
formData.append('_token', CSRF_TOKEN);
var url =$(this).attr("action");
// alert(url);
var type = $(this).attr("method");
$.ajax({
    url:url,
    type:type,
    data:formData,
    processData: false,
    contentType: false,
    success:function(response)
    {
        if(response.status == "true")
        {
            swal({
                text:response.message,
                icon:"success"
            });
            location.reload();
        }else{
            swal({
                text:response.message,
                icon:"error"
            }); 
        }
    }
});     
}
});
var currunt_url = window.location.href;
if(currunt_url.includes('dashboard'))
{
    currunt_url=currunt_url.replace('dashboard','studio');
}
    // datatable
    var table = $('#studioDatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: currunt_url+"/datatable",
            type:"POST",
            data:{
                "_token":CSRF_TOKEN,
              
            }
        },
        columns: [
            { data:"checkbox", name:"checkbox", orderable:false, searchable:false},
            {data: 'sm_id', name: 'sm_id'},
            {data: 'sm_name', name: 'sm_name'},
            {data: 'sm_description', name: 'sm_description'},
            {
                data: 'logo',
                name: 'logo',
                orderable: false,
                searchable: false
            },
            {data: 'admin', name: 'admin'},
            {data: 'status', name: 'status'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "preDrawCallback": function( settings ) {
            // $('.student_checkbox:checked').each(function(){
            //     id.push($(this).val());
            // });
          },
        "drawCallback": function( row, data, index ) {
            // var check = $("input:checkbox#checkAll").is(":checked")?true:false;
            // $("input:checkbox").prop('checked', check);
        }

    });
  
// delete button click
$(document).on("click","button.delete",function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Studio",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            var url =$(this).data("link");
    $.ajax({
        url:url,
        type:"POST",
        data:{
            "_token":CSRF_TOKEN
        },
        success:function(response){
            if(response.status == "true")
            {
                swal({
                    text:response.message,
                    icon:"success"
                });
            }else{
                swal({
                    text:response.message,
                    icon:"error"
                });
            }
            table.ajax.reload();
        }
    });

        } else {
          swal("Studio is safe!");
        }
      });
    
});
    
});