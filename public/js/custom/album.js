$(document).ready(function(){
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    

    var currunt_url = window.location.href;

    var sm_id = '9';
    // datatable
    var table = $('#albumDatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: currunt_url+"/datatable",
            type:"POST",
            data:{
                "_token":CSRF_TOKEN,
                "studio_id":sm_id
            }
        },
        columns: [
            { data:"checkbox", name:"checkbox", orderable:false, searchable:false},
            {data: 'alm_id', name: 'alm_id'},
            {data: 'alm_name', name: 'alm_name'},
            {data: 'alm_description', name:'alm_description'},
            {data: 'alm_link', name: 'alm_link'},
            {data: 'isprotected', name: 'isprotected'},
            {data: 'status', name: 'status'},
            {data: 'images', name: 'images'},
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "preDrawCallback": function( settings ) {
            // $('.student_checkbox:checked').each(function(){
            //     id.push($(this).val());
            // });
          },
        "drawCallback": function( row, data, index ) {
            // var check = $("input:checkbox#checkAll").is(":checked")?true:false;
            // $("input:checkbox").prop('checked', check);
        }

    });
// image view icon click
$(document).on("click","span#viewImages",function(){
    var albumId = $(this).data("albumid");
    $("#imageViewModal").modal('show');
    fetchAndSetImages(albumId);

});
// delete button click
$(document).on("click","button.delete",function(e){
    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Album",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            var url =$(this).data("link");
                $.ajax({
                    url:url,
                    type:"POST",
                    data:{
                        "_token":CSRF_TOKEN
                    },
                    success:function(response){ 
                        
                        if(response.status == "true")
                        {
                            swal({
                                text:response.message,
                                icon:"success"
                            });
                        }else{
                            swal({
                                text:response.message,
                                icon:"error"
                            });
                        }
                        table.ajax.reload();
                    }
                });

        } else {
          swal("Studio is safe!");
        }
      });
    
});
    $("#loader").hide();
    // submit form validation 
    $("#studioAlbumForm").validate({
        errorElement : 'small',
        errorClass: 'form-text text-danger emailError text-capitalize',
    });
    $('#studio_album_name').rules('add', {  
        required: true, 
        messages: {
            required: "Please Enter Album Name...",
        }
    });
    $('#studio_album_discription').rules('add', {  
        required: true, 
        messages: {
            required: "Please enter Album Discription...",
        }
    });
    // form submit 
    $("#studioAlbumForm").on('submit', function(e) {
        var isvalid = $("#studioAlbumForm").valid();
        if (isvalid) {
            e.preventDefault();
            var formData = new FormData($('form')[0]);
            var url =$(this).attr("action");
            var type = $(this).attr("method");
            var imagesArray = new Array(); 
            $("button#removeImage").each(function(index){
                imagesArray.push($(this).data("imageurl"));
            });
            var f = $(this).serializeArray();
            if(imagesArray.length)
            {
                f.push({
                    "name":"albumimages",
                    "value":imagesArray
                });
            }
            $.ajax({
                url:url,
                type:type,
                data:f,
                beforeSend: function() {
                    $("#studio_album_add_btn_text").html('');
                    $("#loader").removeClass('d-none');
                    $("#loader").show();

                },
                success:function(response)
                {
                    $("#studio_album_add_btn_text").html('Submit');
                    $("#loader").addClass('d-none');
                    $("#loader").hide();
                    if(response.status == "true")
                    {
                        swal({
                            title: "Are You Want To Add New Album ?",
                            text: response.message,
                            icon: "success",
                            // buttons: true,
                            buttons: {
                                cancel: {
                                  text: "Yes",
                                  value: true,
                                  visible: true,
                                  className: "text-danger",
                                  closeModal: true,
                                },
                                confirm: {
                                  text: "No",
                                  value: false,
                                  visible: true,
                                  className: "",
                                  closeModal: true
                                }
                              },
                          })
                          .then((willDelete) => {
                            if (willDelete) 
                            {
                                window.location.reload();
                            }else{
                                window.location = ablumListUrl;
                            }
                          });;
                    }else{
                        swal({
                            text:response.message,
                            icon:"error",
                        });
                    }
                }
            });   
        }  
    });
    // fetchAndSetImages();
    // dropzone for album images upload
    $("#upload_loader").hide();
    var url =$("#studioAlbumForm").attr("action");
    var myDropzone = new Dropzone("div.dropzone", {
        url: url+"/dropzone",
        autoProcessQueue : false,
        acceptedFiles : ".png,.jpg,.gif,.bmp,.jpeg",
        addRemoveLinks: true,
        // uploadMultiple:true,
        addRemoveLinks:true,
        init:function(){
            myDropzone = this;
            this.on("processing", function() {
                $("#studio_album_upload_text").text("Uploading...");
                this.options.autoProcessQueue = true;
            });
            $(document).on("click","#upload_images",function(){
                if(myDropzone.getQueuedFiles().length == 0 && myDropzone.getUploadingFiles().length == 0)
                {
                        swal({
                            text:"Please Choose Any One Image...",
                            icon:"error",
                        });
                }else{
                    myDropzone.processQueue();
                }
            });
            this.on("complete", function(){
                if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                {
                    $("#showImages").removeAttr('style');
                    fetchAndSetImages();
                    $("#studio_album_upload_text").text("Upload Images");
                    var _this = this;
                    _this.removeAllFiles();
                    myDropzone.options.autoProcessQueue = false;
                }
            });
        },
        sending: function(file, xhr, formData) {
            formData.append("_token", CSRF_TOKEN);
        }
      });

    
    // set all album images in ShowImage div if uploaded 
    function fetchAndSetImages(albumId = null)
    {
        var baseUrl = window.location.origin;
        // alert();
        if(currunt_url.includes('add'))
        {
            var url =currunt_url;
            $.get(url+'/fetchimage', function(data) {
            if(data.length != 0)
            {
                $("#showImages").removeClass('d-none');
                $("#imagesBox").html("");
                $.each(data, function(key,value){
                    var imageUrl = baseUrl+"/images/album/temp/"+value.name;
                    var imgCintainer = '<div class="d-block m-1 p-0 " id="imageBox">'+
                                    '<img class="img-responsive mainImage" alt="" width="150"  src="'+imageUrl+'" />'+
                                    '<div class="d-block text-center">'+
                                    '<button type="button" class="btn btn-block btn-danger p-1" data-imageurl="'+value.name+'" id="removeImage">Remove</button>'+
                                    '</div>'+ 
                                    '</div>';
                $("#imagesBox").append(imgCintainer);
                });
            }else{
                $("#showImages").hide();
            }
        });
        }else{
            // alert();
            var albumId = albumId;
            var url =currunt_url;
            $.post(url+'/fetchimage',{"_token":CSRF_TOKEN,alb_id:albumId}, function(data) {
                if(data.length != 0)
                {
                    $("#showImages").removeClass('d-none');
                    $("#imagesBox").html("");
                    $.each(data, function(key,value){
                        var imageUrl = baseUrl+"/images/album/"+value.name;
                        var imgCintainer = '<div class="d-block m-1 p-0 " id="imageBox">'+
                                        '<img class="img-responsive mainImage" alt="" width="150"  src="'+imageUrl+'" />'+
                                        '<div class="d-block text-center">'+
                                        '<button type="button" class="btn btn-block btn-danger p-1" data-imageurl="'+value.name+'" id="removeImage">Remove</button>'+
                                        '</div>'+ 
                                        '</div>';
                    $("#imagesBox").append(imgCintainer);
                    });
                }else{
                    $("#showImages").hide();
                }
            });
        }
        // 
                   
    }
    // on click remove image button
    $(document).on("click","#removeImage",function(){
        var imagename = $(this).data("imageurl");
        $.post(url+'/deleteimage',{ "_token":CSRF_TOKEN, imagename: imagename, imagetype: "temp" },function(data){
            if(data != "true")
            {
                swal({
                    text:"Somthing Is Wrong Image Not Deleted...",
                    icon:"error",
                });
            }else{
                fetchAndSetImages();
            }
        });
    });


});