$(document).ready(function(){
    $("form[name='registerForm']").validate({
      rules: {
        am_name: "required",
        am_password: "required",
        am_email: {
          required: true,
          email: true
        },
        um_cnf_password:{
            required: true,
            equalTo: "#password"
        }
        
      },
      messages: {
        am_name: "Please enter your Full Name",
        am_password: "Please enter your Password",
        um_cnf_password:{
            required:"Please Enter Password Again",
            equalTo:"Confirm Password Is not Matched With Password"
        },
        am_email: "Please enter a valid email address",

      },
      errorElement : 'div',
      errorLabelContainer: '.errorTxt',
      submitHandler: function(form) {
        form.submit();
      }
    });
   
});