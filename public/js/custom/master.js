$(document).ready(function(){
    $('#example').DataTable({
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
            // orderable:false
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
    });
});