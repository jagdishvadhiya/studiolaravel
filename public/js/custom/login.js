$(document).ready(function(){

  $("span#loader").hide();
  
    $("form[name='loginForm']").validate({
        rules: {
          am_email: {
            required: true,
            email: true
          },
          am_password:{
              required: true,
          }
        },
        messages: {
            am_email: "Please enter a valid email address",
            am_password: "Please enter your Password",
        },
        errorElement : 'div',
        errorLabelContainer: '.errorTxt',
        submitHandler: function(form) {
            $.ajax({
              url: form.action,
              type: form.method,
              data: $(form).serialize(),
              beforeSend: function(){
                $("span#login_btn_text").hide();
                $("span#loader").show();
               },
              success: function(response) {
                if(response.status == 'true')
                {
                  $("span#login_btn_text").html("Login Success...").show();
                  $("span#loader").hide();
                  setTimeout(function(){
                    window.location = "admin/dashboard";
                  },300);

                }else{
                  $("span#login_btn_text").show();
                  $("span#loader").hide();
                  if(response.errors)
                  {
                    if(response.errors.am_email[0] )
                    {
                      $("#formHelp").html(response.errors.am_email[0]);
                    }
                  }else{
                    swal({
                            text: response.message,
                            icon: "warning",
                        });
                  }
                }
              }            
          });
        }
      
      });
});